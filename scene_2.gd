extends Node2D

var time: float

func _ready():
	time = 0.0
	material.set_shader_parameter("start_time", time)
	material.set_shader_parameter("time", time)

func _process(delta):
	time += delta
	material.set_shader_parameter("time", time)
